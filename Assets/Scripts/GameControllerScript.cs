using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameControllerScript : MonoBehaviour
{
    public InputAction playerControls;
    public static Vector2 moveDirection;
    public float speed;

    public GameObject ball;

    public NewInputActions inputActions;

    // original position of player
    public static float x_pos;
    public static float y_pos;
    public static float z_pos;

    public void Awake()
    {
        inputActions = new NewInputActions();
        x_pos = transform.position.x;
        y_pos = transform.position.y;
        z_pos = transform.position.z;
        /*Debug.Log(y_pos);*/
    }


    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }


    public void Update()
    {
        moveDirection = inputActions.PlayerController.Move.ReadValue<Vector2>();
        /*moveDirection = playerControls.ReadValue<Vector2>();*/

        if (inputActions.PlayerController.Move.IsPressed())
        {
            transform.Translate(new Vector3(moveDirection.x, 0f, moveDirection.y) * speed * Time.deltaTime);
        }

        if (inputActions.PlayerController.Jump.IsPressed())
        {
            transform.Translate(new Vector3(0f, 2f, 0f) * speed * Time.deltaTime);
        }

    }
}
